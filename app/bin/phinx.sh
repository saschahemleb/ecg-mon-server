#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VENDOR_BIN="${DIR}/../vendor/bin"
eval "${VENDOR_BIN}/phinx -c\"${DIR}/../config/phinx.php\" $@"