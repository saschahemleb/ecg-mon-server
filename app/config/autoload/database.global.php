<?php
return [
    'doctrine' => [
        'configuration' => [
            'auto_generate_proxy_classes' => true,
            'proxy_dir' => 'data/cache/EntityProxy',
            'proxy_namespace' => 'EntityProxy',
            'underscore_naming_strategy' => true,
            'entity_paths' => [
                __DIR__ . '/../../src/App/Model',
            ],
        ],
    ],

    'dependencies' => [
        'factories' => [
            Doctrine\ORM\Configuration::class => App\Container\Doctrine\ConfigurationFactory::class,
            Doctrine\ORM\EntityManager::class => App\Container\Doctrine\EntityManagerFactory::class,
        ],
    ],
];