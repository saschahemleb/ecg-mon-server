<?php
use Zend\Expressive\Application;
use Zend\Expressive\Container\ApplicationFactory;
use Zend\Expressive\Helper;

return [
    'dependencies' => [
        'invokables' => [],
        'factories' => [
            Application::class => ApplicationFactory::class
        ],
    ],
];
