<?php
return [
    'events' => [
        'listeners' => [
        ],

        'subscribers' => [],
    ],

    'dependencies' => [
        'factories' => [
            Symfony\Component\EventDispatcher\EventDispatcherInterface::class => App\Event\EventDispatcherFactory::class
        ],
    ],
];