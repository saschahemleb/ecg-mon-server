<?php
use Zend\Expressive\Container\ApplicationFactory;
use Zend\Expressive\Helper;

return [
  'dependencies' => [
    'factories' => [
    ],
  ],
  // This can be used to seed pre- and/or post-routing middleware
  'middleware_pipeline' => [
    // An array of middleware to register. Each item is of the following
    // specification:
    //
    // [
    //  Required:
    //     'middleware' => 'Name or array of names of middleware services and/or callables',
    //  Optional:
    //     'path'     => '/path/to/match', // string; literal path prefix to match
    //                                     // middleware will not execute
    //                                     // if path does not match!
    //     'error'    => true, // boolean; true for error middleware
    //     'priority' => 1, // int; higher values == register early;
    //                      // lower/negative == register last;
    //                      // default is 1, if none is provided.
    // ],
    //
    // While the ApplicationFactory ignores the keys associated with
    // specifications, they can be used to allow merging related values
    // defined in multiple configuration files/locations. This file defines
    // some conventional keys for middleware to execute early, routing
    // middleware, and error middleware.
//        'always' => [
//            'middleware' => [
//                // Add more middleware here that you want to execute on
//                // every request:
//                // - bootstrapping
//                // - pre-conditions
//                // - modifications to outgoing responses
//                Helper\ServerUrlMiddleware::class,
//            ],
//            'priority' => 10000,
//        ],

    'routing' => [
      'middleware' => [
        ApplicationFactory::ROUTING_MIDDLEWARE,
        ApplicationFactory::DISPATCH_MIDDLEWARE,
      ],
      'priority' => 1,
    ],

    'error' => [
      'middleware' => [
        // Add error middleware here.
      ],
      'error' => true,
      'priority' => -10000,
    ],
  ],
];
