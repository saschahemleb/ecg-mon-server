<?php

use Zend\Expressive\ConfigManager\ConfigManager;
use Zend\Expressive\ConfigManager\PhpFileProvider;

$cachedConfigFile = 'data/cache/app_config.php';

$configManager = new ConfigManager([
  App\AppConfig::class,
  Api\ApiConfig::class,
  new PhpFileProvider('config/autoload/{{,*.}global,{,*.}local}.php'),
], $cachedConfigFile);

return new ArrayObject($configManager->getMergedConfig());