<?php
chdir(dirname(__DIR__));

/** @var \Zend\ServiceManager\ServiceManager $container */
$container = include 'container.php';
$config = $container->get('config');

return [
  'paths' => [
    'migrations' => __DIR__ . '/../db/migrations',
    'seeds' => __DIR__ . '/../db/seeds',
  ],
  'environments' => [
      'default_migration_table' => 'migration',
    'production' => [
        'adapter' => preg_replace('^pdo_(\w)^', '$1', $config['doctrine']['connection']['driver']),
        'host' => $config['doctrine']['connection']['host'],
        'port' => $config['doctrine']['connection']['port'],
        'name' => $config['doctrine']['connection']['dbname'],
        'user' => $config['doctrine']['connection']['user'],
        'pass' => $config['doctrine']['connection']['password'],
      'charset' => 'utf8',
    ],
  ],
];