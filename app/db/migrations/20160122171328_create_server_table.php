<?php

use Phinx\Migration\AbstractMigration;

class CreateServerTable extends AbstractMigration
{
    public function up()
    {
        $regularIntLimit = 4294967295;

        $this->table('server', ['id' => false, 'primary_key' => 'id'])
            ->addColumn(
                'id',
                \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_INTEGER,
                ['identity' => true, 'signed' => false, 'limit' => $regularIntLimit]
            )
            ->addColumn(
                'host',
                \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_STRING,
                ['limit' => 100]
            )
            ->addColumn('name', \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_STRING, ['limit' => 100])
            ->addColumn('is_middleman', \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_BOOLEAN, ['default' => false])
            ->addColumn('is_target', \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_BOOLEAN, ['default' => false])
            ->addColumn('fingerprint', \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_STRING, ['limit' => 100])
            ->addColumn('last_seen', \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_TIMESTAMP,
                ['null' => true, 'default' => null])
            ->addTimestamps()
            ->addIndex(['name'])
            ->addIndex(['last_seen'])
            ->save();
    }

    public function down()
    {
        $this->dropTable('server');
    }
}
