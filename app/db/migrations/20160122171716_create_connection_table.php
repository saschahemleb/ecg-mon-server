<?php

use Phinx\Migration\AbstractMigration;

class CreateConnectionTable extends AbstractMigration
{
    public function up()
    {
        $regularIntLimit = 4294967295;
        $smallIntLimit = 65535;

        $this->table('connection', ['id' => false, 'primary_key' => 'id'])
            ->addColumn(
                'id',
                \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_INTEGER,
                ['identity' => true, 'signed' => false, 'limit' => $regularIntLimit]
            )
            ->addColumn(
                'target_server_id',
                \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_INTEGER,
                ['signed' => false, 'limit' => $regularIntLimit]
            )
            ->addColumn(
                'target_server_port',
                \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_INTEGER,
                ['signed' => false, 'limit' => $smallIntLimit]
            )
            ->addColumn(
                'middleman_server_id',
                \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_INTEGER,
                ['signed' => false, 'limit' => $regularIntLimit]
            )
            ->addColumn(
                'middleman_server_port',
                \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_INTEGER,
                ['signed' => false, 'limit' => $smallIntLimit]
            )
            ->addColumn(
                'type',
                \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_ENUM,
                ['values' => ['ssh', 'http', 'https']]
            )
            ->addColumn(
              'is_connected',
              \Phinx\Db\Adapter\AdapterInterface::PHINX_TYPE_BOOLEAN,
              ['default' => false]
            )
            ->addTimestamps()
            ->addForeignKey('target_server_id', 'server')
            ->addForeignKey('middleman_server_id', 'server')
            ->addIndex(['target_server_id', 'target_server_port'], ['name' => 'uniq_id_port', 'unique' => true])
            ->addIndex(['target_server_id'])
            ->addIndex(['middleman_server_id'])
            ->save();
    }

    public function down()
    {
        $this->dropTable('connection');
    }
}
