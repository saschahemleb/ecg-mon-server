<?php

use Phinx\Seed\AbstractSeed;

class ServerSeeder extends AbstractSeed {
  /**
   * Run Method.
   *
   * Write your database seeder using this method.
   *
   * More information on writing seeders is available here:
   * http://docs.phinx.org/en/latest/seeding.html
   */
  public function run() {
      if ($this->hasTable('server')) {
          $this->table('server')
              ->insert([
                  [
                      'id' => 1,
                      'host' => '192.168.2.101',
                      'name' => 'Sascha-PC',
                      'fingerprint' => 'SHA256:Rf2rMUBgsH+7EiHfqwZqBkyJIYzNSxDyrXV52QYK+1g',
                  ],
                  [
                      'id' => 2,
                      'host' => '192.168.2.3',
                      'name' => 'File-Server',
                      'fingerprint' => 'SHA256:xqZzbuUXxnqgyhPnrPxRGrgw7xKt2J1JIekXNXGT8K8',
                  ]
              ])
              ->save();
      }
  }
}
