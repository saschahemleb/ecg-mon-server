<?php namespace Api;

use Api\Middleware\QueryParamsMiddleware;
use Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware;

class ApiConfig
{
    public function __invoke()
    {
        return [
            'dependencies' => [
                'invokables' => [],
                'factories' => [
                    Endpoint\Hydrator\ConnectionHydrator::class => Endpoint\Hydrator\ConnectionHydratorFactory::class,

                    Endpoint\ServerEndpoint::class => Endpoint\ServerEndpointFactory::class,
                    Endpoint\ConnectionEndpoint::class => Endpoint\ConnectionEndpointFactory::class,
                ],
            ],

            'middleware_pipeline' => [
                'api' => [
                    'path' => '/api',
                    'middleware' => [
                        BodyParamsMiddleware::class,
                    ],
                ],
            ],

            'routes' => [
                [
                    'name' => 'api-ping',
                    'path' => '/api/ping',
                    'middleware' => Endpoint\Ping::class,
                    'allowed_methods' => ['GET'],
                ],

                [
                    'name' => 'api-server',
                    'path' => '/api/server[/{id:\d+}]',
                    'middleware' => [
                        Endpoint\ServerEndpoint::class,
                    ],
                    'allowed_methods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
                ],

                [
                    'name' => 'api-connection',
                    'path' => '/api/server/{server_id:\d+}/connections[/{id:\d+}]',
                    'middleware' => [
                        Endpoint\ConnectionEndpoint::class,
                    ],
                    'allowed_methods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
                ]
            ],
        ];
    }
}
