<?php
namespace Api\Endpoint;

use App\Model\Entity\ConnectionInterface;
use App\Model\Entity\ServerInterface;
use App\Service\ConnectionServiceInterface;
use App\Service\ServerServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Hydrator\HydratorInterface;

class ConnectionEndpoint
{
    use Traits\RestResource;
    /**
     * @var ConnectionServiceInterface
     */
    private $connectionService;
    /**
     * @var ServerServiceInterface
     */
    private $serverService;

    /**
     * @var ServerInterface
     */
    private $server;
    /**
     * @var HydratorInterface
     */
    private $connectionHydrator;

    /**
     * ConnectionEndpoint constructor.
     * @param ConnectionServiceInterface $connectionService
     * @param ServerServiceInterface $serverService
     * @param HydratorInterface $connectionHydrator
     */
    public function __construct(
        ConnectionServiceInterface $connectionService,
        ServerServiceInterface $serverService,
        HydratorInterface $connectionHydrator
    ) {

        $this->connectionService = $connectionService;
        $this->serverService = $serverService;
        $this->connectionHydrator = $connectionHydrator;
    }

    protected function init(ServerRequestInterface $request, ResponseInterface $response)
    {
        $serverId = $request->getAttribute('server_id', null);
        if ($serverId === null) {
            throw new \InvalidArgumentException('This endpoint needs a server id');
        }

        $server = $this->serverService->findServer($serverId);
        if ($server === null) {
            throw new \Exception("Can not find server with ID:$serverId");
        }

        $this->server = $server;
    }


    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    protected function index(
        ServerRequestInterface $request,
        /** @noinspection PhpUnusedParameterInspection */
        ResponseInterface $response
    ) {
        return $this->connectionService->findConnectionsFromServer($this->server);
    }

    protected function get(
        ServerRequestInterface $request,
        /** @noinspection PhpUnusedParameterInspection */
        ResponseInterface $response
    ) {
        $id = $request->getAttribute('id', null);
        return $this->connectionService->findConnection($id);
    }

    protected function post(ServerRequestInterface $request, ResponseInterface $response)
    {
        $params = $request->getParsedBody();

        $connection = $this->connectionService->createConnection($params, $this->connectionHydrator);
        $connection->setTargetServer($this->server);

        $this->connectionService->saveConnection($connection);

        return $connection;
    }

    protected function put(ServerRequestInterface $request, ResponseInterface $response)
    {
        $id = $request->getAttribute('id', null);
        $params = $request->getParsedBody();

        /** @var ConnectionInterface $connection */
        $connection = $this->connectionHydrator->hydrate($params, $this->connectionService->findConnection($id));
        $connection->setTargetServer($this->server);

        $this->connectionService->saveConnection($connection);

        return $connection;
    }

    protected function delete(ServerRequestInterface $request, ResponseInterface $response)
    {
        $id = $request->getAttribute('id', null);
        $connection = $this->connectionService->findConnection($id);

        $this->connectionService->deleteConnection($connection);

        return $response->withStatus($noContent = 204);
    }
}
