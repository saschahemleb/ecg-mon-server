<?php
namespace Api\Endpoint\Hydrator;

use App\Service\ServerServiceInterface;
use Zend\Hydrator\HydratorInterface;

class ConnectionHydrator implements HydratorInterface
{
    /**
     * @var ServerServiceInterface
     */
    private $serverService;
    /**
     * @var HydratorInterface
     */
    private $nativeHydrator;

    public function __construct(
        ServerServiceInterface $serverService,
        HydratorInterface $nativeHydrator
    ) {
        $this->serverService = $serverService;
        $this->nativeHydrator = $nativeHydrator;
    }

    public function hydrate(array $data, $object)
    {
        if (isset($data['middleman_server_id'])) {
            $data['middleman_server'] = $this->serverService->findServer($data['middleman_server_id']);
            unset($data['middleman_server_id']);
        }

        if (isset($data['target_server_id'])) {
            $data['target_server'] = $this->serverService->findServer($data['target_server_id']);
            unset($data['target_server_id']);
        }

        return $this->nativeHydrator->hydrate($data, $object);
    }

    /**
     * Extract values from an object
     *
     * @param  object $object
     * @return array
     */
    public function extract($object)
    {
        return $this->nativeHydrator->extract($object);
    }
}