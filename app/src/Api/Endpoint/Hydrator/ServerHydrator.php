<?php
namespace Api\Endpoint\Hydrator;

use Zend\Hydrator\Reflection;

class ServerHydrator extends Reflection
{
    public function hydrate(array $data, $object)
    {
        if (isset($data['last_seen']) && is_string($data['last_seen'])) {
            $data['last_seen'] = new \DateTime($data['last_seen']);
        }
        unset($data['updated_at'], $data['created_at']);

        return parent::hydrate($data, $object);
    }
}
