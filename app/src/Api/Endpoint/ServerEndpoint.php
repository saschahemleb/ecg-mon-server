<?php
namespace Api\Endpoint;

use App\Service\ServerServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Hydrator\HydratorInterface;

class ServerEndpoint
{
    use Traits\RestResource;

    /**
     * @var ServerServiceInterface
     */
    private $serverService;

    /**
     * @var HydratorInterface
     */
    private $serverHydrator;

    /**
     * Server constructor.
     * @param ServerServiceInterface $serverService
     * @param HydratorInterface $serverHydrator
     */
    public function __construct(ServerServiceInterface $serverService, HydratorInterface $serverHydrator)
    {
        $this->serverService = $serverService;
        $this->serverHydrator = $serverHydrator;
    }

    protected function index(

        /** @noinspection PhpUnusedParameterInspection */
        ServerRequestInterface $request,
        /** @noinspection PhpUnusedParameterInspection */
        ResponseInterface $response
    ) {
        $query = $request->getQueryParams();
        if (empty($query)) {
            return $this->serverService->findAllServer();
        } else {
            return $this->serverService->searchServer($query);
        }
    }

    protected function get(
        ServerRequestInterface $request,
        /** @noinspection PhpUnusedParameterInspection */
        ResponseInterface $response
    ) {
        $id = $request->getAttribute('id', null);
        return $this->serverService->findServer($id);
    }

    protected function post(
        ServerRequestInterface $request,
        /** @noinspection PhpUnusedParameterInspection */
        ResponseInterface $response
    ) {
        $params = $request->getParsedBody();
        $server = $this->serverService->createServer($params, $this->serverHydrator);

        $this->serverService->saveServer($server);

        return $server;
    }

    protected function put(
        ServerRequestInterface $request,
        /** @noinspection PhpUnusedParameterInspection */
        ResponseInterface $response
    ) {
        $id = $request->getAttribute('id', null);
        $params = $request->getParsedBody();

        $server = $this->serverService->findServer($id);

        $this->serverHydrator->hydrate($params, $server);

        $this->serverService->saveServer($server);

        return $server;
    }

    protected function delete(ServerRequestInterface $request, ResponseInterface $response)
    {
        $id = $request->getAttribute('id', null);
        $server = $this->serverService->findServer($id);

        $this->serverService->deleteServer($server);

        return $response->withStatus($noContent = 204);
    }
}
