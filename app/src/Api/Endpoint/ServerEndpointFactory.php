<?php
namespace Api\Endpoint;

use App\Service\ServerServiceInterface;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class ServerEndpointFactory implements FactoryInterface
{
  /**
   * Create an object
   *
   * @param  ContainerInterface $container
   * @param  string $requestedName
   * @param  null|array $options
   * @return object
   * @throws ServiceNotFoundException if unable to resolve the service.
   * @throws ServiceNotCreatedException if an exception is raised when
   *     creating a service.
   * @throws ContainerException if any other error occurs
   */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ServerServiceInterface $serverService */
        $serverService = $container->get(ServerServiceInterface::class);
        return new ServerEndpoint(
            $serverService,
            new Hydrator\ServerHydrator()
        );
    }
}
