<?php
namespace Api\Endpoint\Traits;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\JsonResponse;

trait RestResource
{
    protected static $allowedHttpMethods = ['GET', 'POST', 'PATCH', 'PUT', 'DELETE'];

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $httpMethod = $request->getMethod();

        if (!in_array($httpMethod, self::$allowedHttpMethods)) {
            throw new \InvalidArgumentException("Unknown/Unsupported HTTP Method \"$httpMethod\"");
        }

        $this->init($request, $response);

        $function = [$this, strtolower($httpMethod)];
        switch ($httpMethod) {
            case 'GET':
                $id = $request->getAttribute('id', null);
                if ($id === null) {
                    $function[1] = 'index';
                }
                break;

            case 'PATCH':
                $function[1] = 'put';
                break;
        }

        $result = call_user_func($function, $request, $response);

        if (!($result instanceof Response)) {
            $result = new JsonResponse($result);
        }

        if ($out === null) {
            return $result;
        }

        return $out($request, $result);
    }

    protected function init(Request $request, Response $response)
    {

    }

    abstract protected function index(Request $request, Response $response);

    abstract protected function get(Request $request, Response $response);

    abstract protected function post(Request $request, Response $response);

    abstract protected function put(Request $request, Response $response);

    abstract protected function delete(Request $request, Response $response);
}
