<?php
namespace App;

class AppConfig
{
    public function __invoke()
    {
        return [
            'dependencies' => [
                'invokables' => [],
                'factories' => [
                    Service\ServerServiceInterface::class => Service\ServerServiceFactory::class,
                    Service\ConnectionServiceInterface::class => Service\ConnectionServiceFactory::class,
                ],
            ],
        ];
    }
}
