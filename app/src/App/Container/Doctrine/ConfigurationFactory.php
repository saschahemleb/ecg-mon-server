<?php
namespace App\Container\Doctrine;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class ConfigurationFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->has('config') ? $container->get('config') : [];

        $proxyDir = (isset($config['doctrine']['configuration']['proxy_dir'])) ?
            $config['doctrine']['configuration']['proxy_dir'] : 'data/cache/EntityProxy';

        $proxyNamespace = (isset($config['doctrine']['configuration']['proxy_namespace'])) ?
            $config['doctrine']['configuration']['proxy_namespace'] : 'EntityProxy';

        $autoGenerateProxyClasses = (isset($config['doctrine']['configuration']['auto_generate_proxy_classes'])) ?
            $config['doctrine']['configuration']['auto_generate_proxy_classes'] : false;

        $underscoreNamingStrategy = (isset($config['doctrine']['configuration']['underscore_naming_strategy'])) ?
            $config['doctrine']['configuration']['underscore_naming_strategy'] : false;

        $configuration = new Configuration();
        $configuration->setProxyDir($proxyDir);
        $configuration->setProxyNamespace($proxyNamespace);
        $configuration->setAutoGenerateProxyClasses($autoGenerateProxyClasses);
        if ($underscoreNamingStrategy) {
            $configuration->setNamingStrategy(new UnderscoreNamingStrategy());
        }

        // ORM mapping by Annotation
        AnnotationRegistry::registerFile('vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php');
        $configuration->setMetadataDriverImpl(new AnnotationDriver(
            new AnnotationReader(),
            $config['doctrine']['configuration']['entity_paths']
        ));

        return $configuration;
    }
}
