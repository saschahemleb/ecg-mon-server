<?php
namespace App\Event;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class EventDispatcherFactory implements FactoryInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $this->container = $container;
        $config = $container->get('config');
        $eventDispatcher = new EventDispatcher();
        $listeners = isset($config['events']['listeners']) ? $config['events']['listeners'] : array();
        foreach ($listeners as $listenerInfo) {
            $event = $listenerInfo['event'];
            $listener = $this->resolveListenerToDispatcherCallable($listenerInfo['listener']);
            $eventDispatcher->addListener($event, $listener);
        }

        $subscribers = isset($config['events']['subscribers']) ? $config['events']['subscribers'] : array();
        foreach ($subscribers as $subscriber) {
            $eventDispatcher->addSubscriber($this->resolveSubscriber($subscriber));
        }

        return $eventDispatcher;
    }

    private function resolveListenerToDispatcherCallable($listener)
    {
        if (is_callable($listener)) {
            return $listener;
        }

        if (is_string($listener)) {
            if ($this->container->has($listener)) {
                $listener = $this->container->get($listener);
            } elseif (class_exists($listener)) {
                $listener = new $listener;
            }

            if (!method_exists($listener, '__invoke')) {
                throw new ServiceNotCreatedException('Class listener must implement the __invoke function');
            }
        }

        return $listener;
    }

    private function resolveSubscriber($subscriber)
    {
        if (is_string($subscriber)) {
            if ($this->container->has($subscriber)) {
                $subscriber = $this->container->get($subscriber);
            } elseif (class_exists($subscriber)) {
                $subscriber = new $subscriber;
            }
        }

        if (!($subscriber instanceof EventSubscriberInterface)) {
            throw new ServiceNotCreatedException(sprintf('instance must be of type %s',
                EventSubscriberInterface::class));
        }

        return $subscriber;
    }
}