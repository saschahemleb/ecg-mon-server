<?php
namespace App\Model\Entity;

use Doctrine\Orm\Mapping as ORM;

/**
 * Class Connection
 * @package App\Model\Entity
 *
 * @ORM\Entity(repositoryClass="App\Model\Repository\ConnectionRepository")
 */
class Connection implements ConnectionInterface, \JsonSerializable
{
    private static $types = ['ssh', 'http', 'https'];

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Server")
     */
    private $target_server;

    /**
     * @ORM\Column(type="integer")
     */
    private $target_server_port;

    /**
     * @ORM\ManyToOne(targetEntity="Server")
     */
    private $middleman_server;

    /**
     * @ORM\Column(type="integer")
     */
    private $middleman_server_port;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_connected = false;

    /**
     * @param mixed $id
     * @return ConnectionInterface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param ServerInterface $targetServer
     * @return ConnectionInterface
     */
    public function setTargetServer(ServerInterface $targetServer)
    {
        $this->target_server = $targetServer;
        return $this;
    }

    /**
     * @return ServerInterface
     */
    public function getTargetServer()
    {
        return $this->target_server;
    }

    /**
     * @param mixed $targetServerPort
     * @return ConnectionInterface
     */
    public function setTargetServerPort($targetServerPort)
    {
        $this->target_server_port = $targetServerPort;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTargetServerPort()
    {
        return $this->target_server_port;
    }

    public function setMiddlemanServer(ServerInterface $middlemanServer)
    {
        $this->middleman_server = $middlemanServer;
    }

    public function getMiddlemanServer()
    {
        return $this->middleman_server;
    }


    /**
     * @param mixed $middlemanServerPort
     * @return ConnectionInterface
     */
    public function setMiddlemanServerPort($middlemanServerPort)
    {
        $this->middleman_server_port = $middlemanServerPort;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMiddlemanServerPort()
    {
        return $this->middleman_server_port;
    }

    /**
     * @param mixed $type
     * @return ConnectionInterface
     */
    public function setType($type)
    {
        if (!in_array($type, self::$types)) {
            throw new \InvalidArgumentException("Unknown connection type \"$type\"");
        }

        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $isConnected
     * @return bool
     */
    public function setIsConnected($isConnected) {
        $this->is_connected = (bool)$isConnected;
    }

    /**
     * @return bool
     */
    public function getIsConnected() {
        return $this->is_connected;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $targetServer = $this->getTargetServer();
        $middlemanServer = $this->getMiddlemanServer();
        return [
            'id' => $this->getId(),
            'target_server_id' => $targetServer === null ? null : $targetServer->getId(),
            'target_server_port' => $this->getTargetServerPort(),
            'middleman_server_id' => $middlemanServer === null ? null : $middlemanServer->getId(),
            'middleman_server_port' => $this->getMiddlemanServerPort(),
            'type' => $this->getType(),
            'is_connected' => $this->getIsConnected()
        ];
    }
}
