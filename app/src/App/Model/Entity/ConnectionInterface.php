<?php
namespace App\Model\Entity;

/**
 * Class Connection
 * @package App\Model\Entity
 *
 * @ORM\Entity(repositoryClass="App\Model\Repository\ConnectionRepository")
 */
interface ConnectionInterface
{
    /**
     * @param int $id
     * @return ConnectionInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param ServerInterface $targetServer
     * @return ConnectionInterface
     */
    public function setTargetServer(ServerInterface $targetServer);

    /**
     * @return ServerInterface
     */
    public function getTargetServer();

    /**
     * @param $targetServerPort
     * @return ConnectionInterface
     */
    public function setTargetServerPort($targetServerPort);

    /**
     * @return int
     */
    public function getTargetServerPort();

    /**
     * @param ServerInterface $middlemanServer
     * @return ConnectionInterface
     */
    public function setMiddlemanServer(ServerInterface $middlemanServer);

    /**
     * @return ServerInterface
     */
    public function getMiddlemanServer();

    /**
     * @param $middlemanServerPort
     * @return ConnectionInterface
     */
    public function setMiddlemanServerPort($middlemanServerPort);

    /**
     * @return int
     */
    public function getMiddlemanServerPort();

    /**
     * @param string $type
     * @return ConnectionInterface
     */
    public function setType($type);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param $isConnected
     * @return bool
     */
    public function setIsConnected($isConnected);

    /**
     * @return bool
     */
    public function getIsConnected();
}
