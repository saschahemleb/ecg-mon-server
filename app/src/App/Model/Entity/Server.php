<?php
namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Zend\Stdlib\JsonSerializable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Server
 * @package App\Model\Entity
 *
 * @ORM\Entity(repositoryClass="App\Model\Repository\ServerRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Server implements ServerInterface, JsonSerializable
{
    /**
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $host;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_middleman = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_target = false;

    /**
     * @ORM\Column(type="string")
     */
    private $fingerprint;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime|null
     */
    private $last_seen;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime|null
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="Connection", mappedBy="target_server")
     * @var ConnectionInterface[]
     */
    private $connections;

    public function __construct()
    {
        $this->connections = new ArrayCollection();
    }

    /**
     * @param mixed $id
     * @return Server
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $host
     * @return Server
     */
    public function setHost($host)
    {
        if (is_numeric($host)) {
            $host = long2ip($host);
        }

        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $name
     * @return Server
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function setIsMiddleman($isMiddleman)
    {
        $this->is_middleman = (bool)$isMiddleman;
        return $this;
    }

    public function getIsMiddleman()
    {
        return $this->is_middleman;
    }

    public function setIsTarget($isTarget)
    {
        $this->is_target = (bool)$isTarget;
        return $this;
    }

    public function getIsTarget()
    {
        return $this->is_target;
    }

    public function setFingerprint($fingerprint)
    {
        $this->fingerprint = $fingerprint;
        return $this;
    }

    public function getFingerprint()
    {
        return $this->fingerprint;
    }


    public function setLastSeen(\DateTime $lastSeen = null)
    {
        $this->last_seen = $lastSeen;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastSeen()
    {
        return $this->last_seen;
    }

    public function setCreatedAt(\DateTime $createdAt = null)
    {
        return $this;
    }


    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updated_at = $updatedAt;
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function addConnection(ConnectionInterface $connection)
    {
        $this->connections[] = $connection;
    }

    /**
     * @return ConnectionInterface[]
     */
    public function getConnections()
    {
        return $this->connections;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTimestamps()
    {
        $this->setUpdatedAt(new \DateTime());

        if ($this->getCreatedAt() === null) {
            $this->created_at = new \DateTime();
        }
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $lastSeen = $this->getLastSeen();
        $updatedAt = $this->getUpdatedAt();
        return [
            'id' => $this->getId(),
            'host' => $this->getHost(),
            'name' => $this->getName(),
            'is_middleman' => $this->getIsMiddleman(),
            'is_target' => $this->getIsTarget(),
            'fingerprint' => $this->getFingerprint(),
            'last_seen' => $lastSeen === null ? null : $lastSeen->format('Y-m-d H:i:s'),
            'created_at' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
            'updated_at' => $updatedAt === null ? null : $updatedAt->format('Y-m-d H:i:s'),
        ];
    }
}
