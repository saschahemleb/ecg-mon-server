<?php
namespace App\Model\Entity;

interface ServerInterface
{
    /**
     * @param int $id
     * @return ServerInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param string $host
     * @return ServerInterface
     */
    public function setHost($host);

    /**
     * @return string
     */
    public function getHost();

    /**
     * @param string $name
     * @return ServerInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    public function setIsMiddleman($isMiddleman);

    public function getIsMiddleman();

    public function setIsTarget($isTarget);

    public function getIsTarget();

    public function setFingerprint($fingerprint);

    public function getFingerprint();

    public function setLastSeen(\DateTime $lastSeen);

    public function getLastSeen();

    public function setCreatedAt(\DateTime $createdAt = null);

    public function getCreatedAt();

    public function setUpdatedAt(\DateTime $updatedAt);

    public function getUpdatedAt();

    public function addConnection(ConnectionInterface $connection);

    /**
     * @return ConnectionInterface[]
     */
    public function getConnections();
}
