<?php
namespace App\Model\Repository;

use Doctrine\ORM\EntityRepository;

abstract class AbstractRepository extends EntityRepository
{
    protected function saveEntity($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    protected function deleteEntity($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    public function create()
    {
        $entityName = $this->getEntityName();
        return new $entityName;
    }
}
