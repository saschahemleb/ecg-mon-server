<?php
namespace App\Model\Repository;

use App\Model\Entity\ConnectionInterface;
use App\Model\Entity\ServerInterface;

class ConnectionRepository extends AbstractRepository implements ConnectionRepositoryInterface
{

    public function save(ConnectionInterface $connection)
    {
        $this->saveEntity($connection);
    }

    public function delete(ConnectionInterface $connection)
    {
        $this->deleteEntity($connection);
    }

    public function findFromServer(ServerInterface $server)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('c')
            ->from($this->getEntityName(), 'c')
            ->where('c.target_server = :target_server')
            ->setParameter('target_server', $server);

        return $qb->getQuery()->getResult();
    }
}
