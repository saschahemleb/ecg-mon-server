<?php


namespace App\Model\Repository;

use App\Model\Entity\ConnectionInterface;
use App\Model\Entity\ServerInterface;

interface ConnectionRepositoryInterface
{

    public function findAll();

    public function find($id);

    public function findFromServer(ServerInterface $server);

    public function save(ConnectionInterface $connection);

    public function delete(ConnectionInterface $connection);

    public function create();
}
