<?php
namespace App\Model\Repository;

use App\Model\Entity\ServerInterface;

class ServerRepository extends AbstractRepository implements ServerRepositoryInterface
{
    public function save(ServerInterface $server)
    {
        $this->saveEntity($server);
    }

    public function delete(ServerInterface $server)
    {
        $this->deleteEntity($server);
    }

    public function search(array $query)
    {
        $metadata = $this->getClassMetadata();
        $qb = $this->createQueryBuilder('server');
        $where = $qb->expr()->andX();
        foreach ($query as $field => $value) {
            if (!$metadata->hasField($field)) {
                throw new \InvalidArgumentException(sprintf("Unknown field \"%s\"", $field));
            }
            $where->add($qb->expr()->eq("server.$field", ":$field"));
        }
        if ($where->count()) {
            $qb->where($where)->setParameters($query);
        }

        return $qb->getQuery()->getResult();
    }
}
