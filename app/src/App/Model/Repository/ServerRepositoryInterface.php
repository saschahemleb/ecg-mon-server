<?php
namespace App\Model\Repository;

use App\Model\Entity\ServerInterface;

interface ServerRepositoryInterface
{
    public function findAll();

    public function search(array $query);

    public function find($id);

    public function save(ServerInterface $server);

    public function delete(ServerInterface $server);

    public function create();
}
