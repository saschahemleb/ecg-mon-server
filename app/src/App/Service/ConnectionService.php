<?php
namespace App\Service;

use App\Model\Entity\ConnectionInterface;
use App\Model\Entity\ServerInterface;
use App\Model\Repository\ConnectionRepositoryInterface;
use Zend\Hydrator\HydratorInterface;

class ConnectionService implements ConnectionServiceInterface
{
    /**
     * @var ConnectionRepositoryInterface
     */
    private $connectionRepository;

    /**
     * ConnectionService constructor.
     * @param ConnectionRepositoryInterface $connectionRepository
     */
    public function __construct(ConnectionRepositoryInterface $connectionRepository)
    {
        $this->connectionRepository = $connectionRepository;
    }

    /**
     * @return ConnectionInterface[]
     */
    public function findAllConnections()
    {
        return $this->connectionRepository->findAll();
    }

    /**
     * @param $id
     * @return ConnectionInterface
     * @throws \InvalidArgumentException
     */
    public function findConnection($id)
    {
        $connection = $this->connectionRepository->find($id);
        if ($connection === null) {
            throw new \InvalidArgumentException("Unknown connection with ID:$id");
        }
        return $connection;
    }

    public function findConnectionsFromServer(ServerInterface $server)
    {
        return $this->connectionRepository->findFromServer($server);
    }

    /**
     * @param ConnectionInterface $connection
     */
    public function saveConnection(ConnectionInterface $connection)
    {
        return $this->connectionRepository->save($connection);
    }

    /**
     * @param ConnectionInterface $connection
     */
    public function deleteConnection(ConnectionInterface $connection)
    {
        return $this->connectionRepository->delete($connection);
    }

    public function createConnection(array $data, HydratorInterface $hydrator)
    {
        return $hydrator->hydrate($data, $this->connectionRepository->create());
    }
}
