<?php


namespace App\Service;

use App\Model\Entity\ServerInterface;
use App\Model\Entity\ConnectionInterface;
use Zend\Hydrator\HydratorInterface;

interface ConnectionServiceInterface
{
    /**
     * @return ConnectionInterface[]
     */
    public function findAllConnections();

    /**
     * @param $id
     * @return ConnectionInterface
     * @throws \InvalidArgumentException
     */
    public function findConnection($id);

    public function findConnectionsFromServer(ServerInterface $server);

    /**
     * @param ConnectionInterface $connection
     */
    public function saveConnection(ConnectionInterface $connection);

    /**
     * @param ConnectionInterface $connection
     */
    public function deleteConnection(ConnectionInterface $connection);

    /**
     * @param array $data
     * @param HydratorInterface $hydrator
     * @return ConnectionInterface
     */
    public function createConnection(array $data, HydratorInterface $hydrator);
}
