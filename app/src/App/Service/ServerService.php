<?php


namespace App\Service;

use App\Model\Entity\ServerInterface;
use App\Model\Repository\ServerRepositoryInterface;
use Zend\Hydrator\HydratorInterface;

class ServerService implements ServerServiceInterface
{
    /**
     * @var ServerRepositoryInterface
     */
    private $serverRepository;

    public function __construct(ServerRepositoryInterface $serverRepository)
    {
        $this->serverRepository = $serverRepository;
    }

    /**
     * @return ServerInterface[]
     */
    public function findAllServer()
    {
        return $this->serverRepository->findAll();
    }

    public function searchServer(array $searchParams)
    {
        return $this->serverRepository->search($searchParams);
    }


    /**
     * @param $id
     * @return ServerInterface
     * @throws \InvalidArgumentException
     */
    public function findServer($id)
    {
        $server = $this->serverRepository->find($id);
        if ($server === null) {
            throw new \InvalidArgumentException("Server with ID:$id can not be found.");
        }
        return $server;
    }

    /**
     * @param ServerInterface $server
     */
    public function saveServer(ServerInterface $server)
    {
        $this->serverRepository->save($server);
    }

    public function deleteServer(ServerInterface $server)
    {
        $this->serverRepository->delete($server);
    }

    public function createServer(array $data, HydratorInterface $hydrator)
    {
        return $hydrator->hydrate($data, $this->serverRepository->create());
    }
}
