<?php
namespace App\Service;

use App\Model\Entity\Server;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class ServerServiceFactory implements FactoryInterface
{
  /**
   * Create an object
   *
   * @param  ContainerInterface $container
   * @param  string $requestedName
   * @param  null|array $options
   * @return object
   * @throws ServiceNotFoundException if unable to resolve the service.
   * @throws ServiceNotCreatedException if an exception is raised when
   *     creating a service.
   * @throws ContainerException if any other error occurs
   */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $em */
        $em = $container->get(EntityManager::class);

        /** @var \App\Model\Repository\ServerRepositoryInterface $serverRepo */
        $serverRepo = $em->getRepository(Server::class);

        return new ServerService($serverRepo);
    }
}
