<?php
namespace App\Service;

use App\Model\Entity\ServerInterface;
use Zend\Hydrator\HydratorInterface;

interface ServerServiceInterface
{
    /**
     * @return ServerInterface[]
     */
    public function findAllServer();

    public function searchServer(array $searchParams);

    /**
     * @param $id
     * @return ServerInterface
     * @throws \InvalidArgumentException
     */
    public function findServer($id);

    /**
     * @param ServerInterface $server
     */
    public function saveServer(ServerInterface $server);

    /**
     * @param ServerInterface $server
     */
    public function deleteServer(ServerInterface $server);

    public function createServer(array $data, HydratorInterface $hydrator);
}
